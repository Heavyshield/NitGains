module android15

import nitGains
import android 



redef class Button

	init do set_android_style(native, app.native_activity)

	private fun set_android_style(java_button: NativeButton, activity: NativeActivity)
		in "Java" `{

			int back_color_id = 0;
			back_color_id = R.color.WhiteSmoke;
			int text_color_id = 0;
			text_color_id = R.color.Black;
			java_button.setBackgroundColor(back_color_id);
			java_button.setGravity(android.view.Gravity.CENTER);
			java_button.setAllCaps(false);
			java_button.setTextColor(text_color_id);


	`}
end

redef class Label

	init do set_android_style(native, app.native_activity)

	private fun set_android_style(java_label: NativeTextView, activity: NativeActivity)
		in "Java" `{

			int back_color_id = 0;
			back_color_id = R.color.White;
			int text_color_id = 0;
			text_color_id = R.color.Black;
			java_label.setBackgroundColor(back_color_id);
			java_label.setGravity(android.view.Gravity.CENTER);
			java_label.setAllCaps(false);
			java_label.setTextColor(text_color_id);


	`}
end





